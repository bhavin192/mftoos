// mftoos: Mi Fit exported CSV to openScale compatible CSV
package main

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"time"
)

const (
	// values from MiFit CSV format
	mfTimestamp = iota
	mfWeight
	mfHeight
	mfBmi
	mfFatrate
	mfBodywater
	mfBonemass
	mfMetabolism
	mfMusclerate
	mfVisceralfat
	mfImpedance

	mfFields = 11
)

const (
	// values from openScale CSV format
	osBiceps = iota
	osBone
	osCaliper1
	osCaliper2
	osCaliper3
	osCalories
	osChest
	osComment
	osDatetime
	osFat
	osHip
	osLbm
	osMuscle
	osNeck
	osThigh
	osVisceralfat
	osWaist
	osWater
	osWeight

	osFields = 19
)

func main() {
	if len(os.Args) != 3 {
		fmt.Printf("Usage: %s [INPUT_FILE] [OUTPUT_FILE]\n", os.Args[0])
		fmt.Printf("Convert Mi Fit exported INPUT_FILE csv to openScale compatible OUTPUT_FILE csv.\n")
		os.Exit(1)
	}

	mfFile := os.Args[1]
	osFile := os.Args[2]

	f, err := ioutil.ReadFile(mfFile)
	if err != nil {
		log.Fatalf("error while reading %q: %v", mfFile, err)
	}

	csvReader := csv.NewReader(bytes.NewReader(f))
	var osBuffer bytes.Buffer
	csvWriter := csv.NewWriter(&osBuffer)

	osRecord := []string{
		"biceps", "bone", "caliper1", "caliper2",
		"caliper3", "calories", "chest", "comment",
		"dateTime", "fat", "hip", "lbm",
		"muscle", "neck", "thigh", "visceralFat",
		"waist", "water", "weight"}

	if err := csvWriter.Write(osRecord); err != nil {
		log.Fatalf("error while writing heading record %v: %v\n", osRecord, err)
	}

	// Read the record header
	csvReader.Read()

	for {
		record, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("error while reading CSV record: %v", err)
		}

		// osRecord := make([]string, osFields, osFields)
		osRecord[osDatetime] = convertTimeStamp(record[mfTimestamp])
		osRecord[osWeight] = record[mfWeight]
		osRecord[osFat] = record[mfFatrate]
		osRecord[osWater] = record[mfBodywater]
		osRecord[osBone] = record[mfBonemass]
		osRecord[osMuscle] = record[mfMusclerate]
		osRecord[osVisceralfat] = record[mfVisceralfat]
		osRecord[osComment] = fmt.Sprintf(
			"Height: %s; BMI: %s; Metabolism: %s; Impedance: %s",
			record[mfHeight],
			record[mfBmi],
			record[mfMetabolism],
			record[mfImpedance])
		osRecord[osBiceps] = "0.0"
		osRecord[osCaliper1] = "0.0"
		osRecord[osCaliper2] = "0.0"
		osRecord[osCaliper3] = "0.0"
		osRecord[osCalories] = "0.0"
		osRecord[osChest] = "0.0"
		osRecord[osFat] = "0.0"
		osRecord[osHip] = "0.0"
		osRecord[osLbm] = "0.0"
		osRecord[osNeck] = "0.0"
		osRecord[osThigh] = "0.0"
		osRecord[osWaist] = "0.0"

		if err := csvWriter.Write(osRecord); err != nil {
			log.Printf("error while writing record %v: %v\n", osRecord, err)
		}
	}

	csvWriter.Flush()
	if err := csvWriter.Error(); err != nil {
		log.Fatalf("error flushing the CSV writer: %v", err)
	}

	if err := ioutil.WriteFile(osFile, osBuffer.Bytes(), 0644); err != nil {
		log.Fatalf("error writing to file %s: %v", osFile, err)
	}

}

// Convert Unix timestamp string to "DD.MM.YYYY HH:MM"
func convertTimeStamp(s string) string {
	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		log.Printf("error converting timestamp %q to int: %v\n", s, err)
		return ""
	}
	t := time.Unix(i, 0)
	return t.Format("02.01.2006 15:04")
}
