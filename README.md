# mftoos: Mi Fit CSV format to openScale

This is small utility to convert CSV records exported from Mi Fit to
[openScale](https://f-droid.org/en/packages/com.health.openscale/) CSV
format.

## How to use
- Make sure you have `go` installed on your machine
- Install mftoos with go get
  ```sh
  $ go get -u gitlab.com/bhavin192/mftoos
  ```
- Converting records from file BODY_2390029.csv and save them as
  oscale.csv
  ```sh
  $ mftoos BODY_2390029.csv oscale.csv
  ```
- Importing in openScale application:  
  In the application go to 'More options' (top right corner) and then
  click on Import

## Licensing

mftoos is licensed under GNU General Public License v3.0. See
[LICENSE](./LICENSE) for the full license text.

